package com.pentacog.fetchat;

import android.app.Application;

import com.pentacog.fetchat.models.Chatter;
import com.pentacog.fetchat.models.Conversation;
import com.pentacog.fetchat.models.Roster;
import com.pentacog.fetchat.networking.ApiClient;

import java.util.HashMap;

public class FetChatApplication extends Application {

    public static final String TAG_PREFIX = "FC:";

    public static final String URL_BASE = "https://fetlife.com";
    public static final String URL_HOME = "/home/v4";
    public static final String URL_GET_HISTORY = "/chat/history.json?sid=%s&with_user_id=%d";
    public static final String URL_DATA = "/chat/data.json";

    public static final String ACTION_MESSAGE = "com.pentacog.fetchat.ACTION_MESSAGE";
    public static final String EXTRA_USER_ID = "com.pentacog.fetchat.EXTRA_USER_ID";

    private Roster mRoster;
    private DataHandler mDataHandler;
    private HashMap<Long, Conversation> mConversations;
    private Chatter mUser;

    private static int mSeq;

    @Override
    public void onCreate() {
        super.onCreate();

        mConversations = new HashMap<>();
//        PersistentCookieStore cookieStore = new PersistentCookieStore(this);
//        CookieHandler.setDefault(cookieStore);

        ApiClient.initialize(this);

        mRoster = new Roster();

        //TODO:Move this to a service
        mDataHandler = new DataHandler(this);

    }

    public Roster getRoster() {
        return mRoster;
    }

    public DataHandler getDataHandler() {
        return mDataHandler;
    }

    public static void setSeq(int seq) {
        mSeq = seq;
    }

    public static int nextSeq() {
        int seq = mSeq;
        mSeq = seq++;

        return seq;
    }

    public Conversation findOrCreateChat(Long userId) {
        Conversation convo = mConversations.get(userId);

        if (convo == null) {
            convo = new Conversation(userId);
            mConversations.put(userId, convo);
        }

        return convo;
    }

    public Chatter getUser() {
        return mUser;
    }

    public void setUser(Chatter user) {
        this.mUser = user;
    }

}

package com.pentacog.fetchat.networking;

import android.util.Log;

import com.pentacog.fetchat.FetChatApplication;
import com.pentacog.fetchat.models.Chatter;
import com.pentacog.fetchat.models.Message;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class WebServicesHandler {

    private final static ExecutorService executor = Executors.newFixedThreadPool(4);
    private static final String TAG = WebServicesHandler.class.getSimpleName();

    private static String chat_sid;

    public static Future login(final String username, final String password, final LoginResponseHandler responseHandler) {
        return executor.submit(new Runnable() {
            @Override
            public void run() {
                ApiClient.loginRequest(username, password, new ApiClient.HttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Object response) {
                        responseHandler.onSuccess(statusCode, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Error error) {
                        responseHandler.onFailure(statusCode, error);
                    }
                });
            }
        });
    }

    /**
     * Runs after initial login to fetch current CSRF token and user data.
     * @param responseHandler
     * @return
     */
    public static Future postSession(final LoginResponseHandler responseHandler) {
        return executor.submit(new Runnable() {
            @Override
            public void run() {
                ApiClient.getCSRFToken(new ApiClient.HttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Object response) {
                        ApiClient.sessionRequest(new ApiClient.HttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Object response) {
                                Log.d(TAG, "postSession response:" + statusCode + " " + response.toString());

                                JSONObject json = (JSONObject) response;

                                try {
                                    chat_sid = json.getString("sid");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                Chatter chatter = new Chatter(json);

                                responseHandler.onSuccess(statusCode, chatter);
                            }

                            @Override
                            public void onFailure(int statusCode, Error error) {
                                responseHandler.onFailure(statusCode, error);
                            }
                        });
                    }

                    @Override
                    public void onFailure(int statusCode, Error error) {
                        responseHandler.onFailure(statusCode, error);
                    }
                });

            }
        });
    }

    public static Future getData(final JSONResponseHandler jsonResponseHandler) {
        return executor.submit(new Runnable() {
            @Override
            public void run() {
                String url = "https://push03.fetlife.com/data.json?sid=" + chat_sid;
                ApiClient.get(url, new ApiClient.HttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Object response) {
                        jsonResponseHandler.onSuccess(statusCode, response);
                    }

                    @Override
                    public void onFailure(int statusCode, Error error) {
                        jsonResponseHandler.onFailure(statusCode, error);
                    }
                });
            }

        });
    }

    //{"chat":{"box_open":2312021},"sid":"00050897-042f-4a27-ada1-28e7cdc94174"}:""

    public static Future postBoxChange(final long userId, final LoginResponseHandler responseHandler) {
        return executor.submit(new Runnable() {
            @Override
            public void run() {

                String url = FetChatApplication.URL_BASE + FetChatApplication.URL_DATA;

                JSONObject root = new JSONObject();
                JSONObject chat = new JSONObject();
                try {
                    root.put("chat", chat);
                    root.put("sid", chat_sid);
                    chat.put("box_open", userId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                ApiClient.post(url, root, new ApiClient.HttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Object response) {
                        responseHandler.onSuccess(statusCode, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Error error) {
                        responseHandler.onFailure(statusCode, error);
                    }
                });

            }
        });
    }

    public static Future postMessage(final Message message) {
        return executor.submit(new Runnable() {
            @Override
            public void run() {
                String url = FetChatApplication.URL_BASE + FetChatApplication.URL_DATA;

                JSONObject root = new JSONObject();
                JSONObject chat = new JSONObject();
                try {
                    root.put("chat", chat);
                    root.put("sid", chat_sid);
                    chat.put("message", message.toJson());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                ApiClient.post(url, root, new ApiClient.HttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Object response) {
//                        responseHandler.onSuccess(statusCode, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Error error) {
//                        responseHandler.onFailure(statusCode, error);
                    }
                });
            }
        });
    }

    public static Future getHistory(final long userId, final HistoryResponseHandler historyResponseHandler) {
        return executor.submit(new Runnable() {
            @Override
            public void run() {
                String url = "https://fetlife.com/chat/history.json?sid=" + chat_sid + "&with_user_id=" + userId;
                ApiClient.get(url, new ApiClient.HttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Object response) {
                        Log.d(TAG, "getHistory response:" + statusCode + " " + response.toString());

                        ArrayList<Message> messages = new ArrayList<>();

                        JSONObject historyJson = (JSONObject) response;
                        JSONArray messagesJson = historyJson.optJSONArray("history");

                        if (messagesJson != null) {

                            for (int i = 0; i < messagesJson.length(); i++) {
                                JSONObject messageJson = messagesJson.optJSONObject(i);

                                if (messageJson != null) {

                                    try {
                                        Message message = new Message(messageJson);
                                        messages.add(message);
                                    } catch (JSONException e) {
                                        Log.e(TAG, "getHistory, failed to parse message: " + e.getLocalizedMessage());
                                    }
                                }
                            }
                        }
                        historyResponseHandler.onSuccess(statusCode, messages);
                    }

                    @Override
                    public void onFailure(int statusCode, Error error) {
                        historyResponseHandler.onFailure(statusCode, error);
                    }
                });
            }

        });

    }

    public interface JSONResponseHandler {
        void onSuccess(int statusCode, Object jsonResponse);
        void onFailure(int statusCode, Error error);
    }

    public interface LoginResponseHandler {
        void onSuccess(int statusCode, Chatter chatter);
        void onFailure(int statusCode, Error error);
    }

    public interface HistoryResponseHandler {
        void onSuccess(int statusCode, ArrayList<Message> messages);
        void onFailure(int statusCode, Error error);
    }

}

package com.pentacog.fetchat.networking;

import android.content.Context;
import android.util.Log;

import com.pentacog.fetchat.FetChatApplication;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ApiClient {

    private static final String TAG = ApiClient.class.getSimpleName();

    private static OkHttpClient client;
    private static String csrfToken;

    public static void initialize(Context context) {
        client = new OkHttpClient();

        CookieManager cmrCookieMan = new CookieManager(new PersistentCookieStore(context), CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(cmrCookieMan);

        client.setCookieHandler(cmrCookieMan);
        client.setConnectTimeout(15, TimeUnit.SECONDS);
        client.setReadTimeout(100, TimeUnit.SECONDS);
    }

    private static OkHttpClient getClient() {
        if (client == null) {
            throw new IllegalStateException("ApiClient must be initialized before use");
        }
        return client;
    }


    protected static void get(String url, HttpResponseHandler responseHandler) {
        performRequest("GET", url, null, responseHandler);
    }

    protected static void put(String url, JSONObject params, HttpResponseHandler responseHandler) {
        performRequest("PUT", url, params, responseHandler);
    }

    protected static void post(String url, JSONObject params, HttpResponseHandler responseHandler) {
        performRequest("POST", url, params, responseHandler);
    }

    protected static void post(String url, HttpResponseHandler responseHandler) {
        performRequest("POST", url, null, responseHandler);
    }

    protected static void loginRequest(String username, String password, HttpResponseHandler responseHandler) {
        RequestBody formBody = new FormEncodingBuilder()
                .add("utf8", "✓")
                .add("nickname_or_email", username)
                .add("password", password)
                .add("commit", "Login+to+FetLife")
                .build();

        Request request = new Request.Builder()
                .url("https://fetlife.com/session")
                .post(formBody)
                .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                .header("Accept-Language", "en-US,en;q=0.5")
                .header("Accept-Encoding", "utf-8")
                .header("Referer", "https://fetlife.com/login")
                .build();

        Response response;
        try {
            response = getClient().newCall(request).execute();
            if (!response.isSuccessful()) {
                responseHandler.onFailure(response.code(), new Error("Request unsuccessful: " + response.code() + " " + response.message()));
            } else {
                responseHandler.onSuccess(response.code(), response.message());
            }
        } catch (IOException e) {
            responseHandler.onFailure(418, new Error("Request unsuccessful: " + e.getLocalizedMessage()));
        }
    }

    protected static void sessionRequest(HttpResponseHandler responseHandler) {
        RequestBody formBody = new FormEncodingBuilder()
                .add("path", "/home/v4")
                .build();

        Request request = new Request.Builder()
                .url("https://fetlife.com/streams.json")
                .post(formBody)
                .header("Host", "fetlife.com")
                .header("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0")
                .header("Accept", "application/json, text/javascript, */*; q=0.01")
                .header("Accept-Language", "en-US,en;q=0.5")
                .header("Accept-Encoding", "utf-8")
                .header("Referer", "https://fetlife.com/home/v4")
                .header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
                .header("X-Csrf-Token", csrfToken)
                .header("X-Requested-With", "XMLHttpRequest")
                .header("Cookie", getCookieHeader())
                .build();

        Response response;
        try {
            response = getClient().newCall(request).execute();
            if (!response.isSuccessful()) {
                responseHandler.onFailure(response.code(), new Error("Request unsuccessful: " + response.code() + " " + response.message()));
            } else {
                Object json = null;
                ResponseBody responseBody = response.body();
                if (responseBody != null) {
                    String b = responseBody.string();

                    if (b.startsWith("{")) {
                        try {
                            json = new JSONObject(b);
                        } catch (JSONException e) {
                            responseHandler.onFailure(418, new Error("Parsing JSON String Failed: " + e.getLocalizedMessage()));
                        }
                    } else {
                        try {
                            json = new JSONArray(b);
                        } catch (JSONException e) {
                            responseHandler.onFailure(418, new Error("Parsing JSON String Failed: " + e.getLocalizedMessage()));
                        }
                    }
                }
                if (responseHandler != null && json != null) {
                    responseHandler.onSuccess(response.code(), json);
                }
            }
        } catch (IOException e) {
            responseHandler.onFailure(418, new Error("Request unsuccessful: " + e.getLocalizedMessage()));
        }
    }

    protected static void getCSRFToken(HttpResponseHandler responseHandler) {
        Request request = new Request.Builder()
                .method("GET", null)
                .url(FetChatApplication.URL_BASE + FetChatApplication.URL_HOME)
                .build();

        Response response;
        try {
            response = getClient().newCall(request).execute();

            if (response.isSuccessful()) {
                ResponseBody responseBody = response.body();
                if (responseBody != null) {
                    String body = responseBody.string();
                    Pattern p = Pattern.compile("meta name=\"csrf-token\" content=\"(.+?)\" ");
                    Matcher m = p.matcher(body);
                    if(m.find()) {
                        csrfToken = m.group(1);
                        responseHandler.onSuccess(response.code(), csrfToken);
                    } else {
                        responseHandler.onFailure(418, new Error("No CSRF Token Found"));
                    }
                }
            }
        } catch (IOException e) {
            Log.d(TAG, "Request (" + request.urlString() + ") failed: " + e.getLocalizedMessage());
            if (responseHandler != null) {
                responseHandler.onFailure(418, new Error(e.getMessage()));
            }
        }
    }

    private static void performRequest(String method, String url, JSONObject params, HttpResponseHandler responseHandler) {

        MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = null;

        if (params != null) {
            body = RequestBody.create(mediaType, params.toString());
        }

        Request request = new Request.Builder()
                .method(method, body)
                .url(url)
                .header("Host", "fetlife.com")
                .header("Accept", "application/json, text/javascript, */*; q=0.01")
                .header("Accept-Language", "en-US,en;q=0.5")
                .header("Accept-Encoding", "utf-8")
                .header("Connection", "Keep-alive")
                .header("Content-Type", "application/json; charset=UTF-8")
                .header("X-Csrf-Token", csrfToken)
                .header("X-Requested-With", "XMLHttpRequest")
                .header("Cookie", getCookieHeader())
                .build();

        Response response;
        try {
            response = getClient().newCall(request).execute();
            if (response.code() >= 300) {
                Log.d(TAG, "Request unsuccessful: " + response.code() + " " + response.message());
                if (responseHandler != null) {
                    responseHandler.onFailure(response.code(), new Error("Request unsuccessful: " + response.code() + " " + response.message()));
                }
            } else {
                Object jsonResult = null;
                ResponseBody responseBody = response.body();

                if (responseBody != null) {
                    String b = responseBody.string();
                    if (b.charAt(0) == '[') {
                        try {
                            jsonResult = new JSONArray(b);
                        } catch (JSONException e) {
                            Log.e(TAG, "error making JSONArray " + e.getLocalizedMessage());
                            jsonResult = new JSONArray();
                        }
                    } else if (b.charAt(0) == '{') {
                        try {
                            jsonResult = new JSONObject(b);
                        } catch (JSONException e) {
                            Log.e(TAG, "error making JSONObject " + e.getLocalizedMessage());
                            jsonResult = new JSONObject();
                        }
                    }

                }
                if (responseHandler != null) {
                    responseHandler.onSuccess(response.code(), jsonResult);
                }
            }
        } catch (IOException e) {
            Log.d(TAG, "Request (" + request.urlString() + ") failed: " + e.getLocalizedMessage());
            if (responseHandler != null) {
                responseHandler.onFailure(418, new Error(e.getMessage()));
            }
        }
    }

    public static void getHistory(String sid, String userId, HttpResponseHandler responseHandler) {

    }

    private static String getCookieHeader() {
        CookieManager cookieManager = (CookieManager) client.getCookieHandler();
        List<HttpCookie> cookies = cookieManager.getCookieStore().getCookies();
        StringBuilder stringBuilder = new StringBuilder();

        for (HttpCookie cookie: cookies) {
            stringBuilder
                    .append(cookie.getName())
                    .append("=")
                    .append(cookie.getValue());


            if (cookies.size() > 0 && cookies.get(cookies.size()-1) != cookie) {
                stringBuilder.append("; ");
            }

        }

        return stringBuilder.toString();
    }

    protected interface HttpResponseHandler {
        void onSuccess(int statusCode, Object response);
        void onFailure(int statusCode, Error error);
    }

}
package com.pentacog.fetchat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.pentacog.fetchat.models.Conversation;
import com.pentacog.fetchat.models.Message;
import com.pentacog.fetchat.models.Roster;
import com.pentacog.fetchat.networking.WebServicesHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Affian on 6/12/2014.
 */
public class DataHandler {

    private static final String TAG = FetChatApplication.TAG_PREFIX + DataHandler.class.getSimpleName();

    private Context context;

    private boolean polling = false;

    public DataHandler(Context c) {
        context = c;
    }

    private void handleData(Object json) throws JSONException {
        Log.d(TAG, "Data: " + json.toString());


        JSONArray rootArray = (JSONArray) json;

        for (int i = 0; i < rootArray.length(); i++) {
            JSONObject rootObject = rootArray.getJSONObject(i);

            JSONArray chatArray = rootObject.getJSONArray("chat");

            for (int j = 0; j < chatArray.length(); j++) {
                JSONObject chatObject = chatArray.getJSONObject(j);

                if (!chatObject.isNull("chat_state")) {
                    handleBootstrap(chatObject.getJSONObject("chat_state"));
                } else if (!chatObject.isNull("roster")) {
                    handleRoster(chatObject.getJSONArray("roster"));
                } else if (!chatObject.isNull("chatter")) {
                    handleChatter(chatObject.getJSONObject("chatter"));
                } else if (!chatObject.isNull("box_open")) {
                    handleBoxOpen(chatObject.getJSONObject("box_open"));
                } else if (!chatObject.isNull("deactivate_conversation")) {

                } else if (!chatObject.isNull("message")) {
                    handleMessage(chatObject.getJSONObject("message"));
                } else if (!chatObject.isNull("conversation")) {
                    handleConversation(chatObject.getJSONObject("conversation"));
                } else if (!chatObject.isNull("roster_remove")) {
                    handleRosterRemove(chatObject.getJSONObject("roster_remove"));
                } else if (!chatObject.isNull("seen_conversation")) {

                } else if (!chatObject.isNull("seen_notifications")) {

                } else if (!chatObject.isNull("set_status")) {

                }
            }

        }

    }

    private void handleRosterRemove(JSONObject roster_remove) {

    }

    private void handleConversation(JSONObject conversation) {

    }

    private void handleMessage(JSONObject jsonMessage) {
        FetChatApplication application = (FetChatApplication) context.getApplicationContext();

        Message message = null;
        try {
            message = new Message(jsonMessage);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (message != null) {
            long convoWith = message.getFrom() == application.getUser().getUserId() ? message.getTo() : message.getFrom();
            Conversation convo = application.findOrCreateChat(convoWith);
            convo.addMessage(message);
            FetChatApplication.setSeq(message.getSeq() + 1);

            Bundle extras = new Bundle();
            extras.putLong(FetChatApplication.EXTRA_USER_ID, convoWith);

            broadcastUpdate(FetChatApplication.ACTION_MESSAGE, extras);
        }


    }

    private void handleBoxOpen(JSONObject box_open) {

    }

    private void handleChatter(JSONObject chatterJson) {
        FetChatApplication application = (FetChatApplication) context.getApplicationContext();
        Roster roster = application.getRoster();

        roster.updateChatter(chatterJson);
    }

    private void handleRoster(JSONArray rosterJson) {
        FetChatApplication application = (FetChatApplication) context.getApplicationContext();
        Roster roster = application.getRoster();

        roster.updateRoster(rosterJson);
    }

    private void handleBootstrap(JSONObject chat_state) {
//        FetChatApplication application = (FetChatApplication) context.getApplicationContext();
        String status = chat_state.optString("status");
        //TODO: Update User's online status.

        if (!chat_state.isNull("roster")) {
            handleRoster(chat_state.optJSONArray("roster"));
        }
        if (!chat_state.isNull("seq")) {
            FetChatApplication.setSeq(chat_state.optInt("seq"));
        }
        //TODO: _reallyHandleBootstrap, generate conversation objects
    }

    public void startDataPolling() {
        if (!polling) {
            polling = true;
            WebServicesHandler.getData(new DataResponseHandler());
        }
    }

    public boolean isPolling() {
        return polling;
    }

    private void broadcastUpdate(String action, Bundle extras) {
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
        Intent intent = new Intent(action);
        intent.putExtras(extras);
        broadcastManager.sendBroadcast(intent);
    }

    private class DataResponseHandler implements  WebServicesHandler.JSONResponseHandler {

        @Override
        public void onSuccess(int statusCode, Object jsonResponse) {
            try {
                handleData(jsonResponse);
            } catch (JSONException e) {
                Log.e(TAG, "Failed to parse Data JSON: " + e.getLocalizedMessage());
            }
            polling = false;
            startDataPolling();
        }

        @Override
        public void onFailure(int statusCode, Error error) {
            polling = false;
            Log.d(TAG, "Data polling failed... aborting. " + statusCode + " " + error.getLocalizedMessage());
        }
    }

}

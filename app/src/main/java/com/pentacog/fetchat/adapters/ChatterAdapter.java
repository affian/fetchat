package com.pentacog.fetchat.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pentacog.fetchat.R;
import com.pentacog.fetchat.models.Chatter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ChatterAdapter extends RecyclerView.Adapter<ChatterAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<Chatter> mDataset;

    private Comparator<Chatter> mComparator = new Comparator<Chatter>() {
        @Override
        public int compare(Chatter lhs, Chatter rhs) {
            boolean leftIsOnline = isOnline(lhs);
            boolean rightIsOnline = isOnline(rhs);

            if (leftIsOnline && !rightIsOnline) {
                return -1;
            } else if (!leftIsOnline && rightIsOnline) {
                return 1;
            } else {
                return lhs.getNickname().compareToIgnoreCase(rhs.getNickname());
            }
        }

        private boolean isOnline(Chatter chatter) {
            return Chatter.STATUS_AVAILABLE.equalsIgnoreCase(chatter.getStatus());
        }
    };

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mTextView;
        public ImageView mImageView;
        public ImageView mStatusView;
        public ViewHolder(View v) {
            super(v);
            mTextView = (TextView) v.findViewById(R.id.user_name);
            mImageView = (ImageView) v.findViewById(R.id.avatar_image_view);
            mStatusView = (ImageView) v.findViewById(R.id.status_image_view);
        }
    }

    public ChatterAdapter(Context context, ArrayList<Chatter> myDataset) {
        mContext = context;
        setDataset(myDataset);
    }

    private void setDataset(ArrayList<Chatter> mDataset) {
        Collections.sort(mDataset, mComparator);
        this.mDataset = mDataset;
    }

    public void updateDataSet(ArrayList<Chatter> dataset) {
        setDataset(dataset);
        notifyDataSetChanged();
    }

    @Override
    public ChatterAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_cell_user, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Chatter chatter = mDataset.get(position);
        holder.mTextView.setText(chatter.getNickname());
        Picasso.with(mContext).load(chatter.getAvatarUrl()).into(holder.mImageView);

        if (Chatter.STATUS_AVAILABLE.equalsIgnoreCase(chatter.getStatus())) {
            holder.mStatusView.setVisibility(View.VISIBLE);
        } else {
            holder.mStatusView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}
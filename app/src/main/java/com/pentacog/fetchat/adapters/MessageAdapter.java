package com.pentacog.fetchat.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pentacog.fetchat.FetChatApplication;
import com.pentacog.fetchat.R;
import com.pentacog.fetchat.models.Chatter;
import com.pentacog.fetchat.models.Message;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<Message> mDataset;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;
        public TextView timestampTextView;
        public ImageView avatarImageView;
        public ViewHolder(View v) {
            super(v);
            mTextView = (TextView) v.findViewById(R.id.message_text_view);
            timestampTextView = (TextView) v.findViewById(R.id.timestamp_text_view);
            avatarImageView = (ImageView) v.findViewById(R.id.avatar_image_view);
        }
    }

    public MessageAdapter(Context context, ArrayList<Message> myDataset) {
        mDataset = myDataset;
        mContext = context;
        setHasStableIds(true);
    }

    @Override
    public MessageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_cell_message, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Message message = mDataset.get(position);
        holder.mTextView.setText(message.getBody());
        holder.timestampTextView.setText(
                DateUtils.getRelativeTimeSpanString(mContext, message.getDate().getTime(), false));
//                    DateUtils.getRelativeTimeSpanString(message.getDate().getTime(), System.currentTimeMillis(), DateUtils.DAY_IN_MILLIS));

        FetChatApplication application = (FetChatApplication) mContext.getApplicationContext();
        Chatter chatter = application.getRoster().getChatterByUserId(message.getFrom());
        if (chatter != null) {
            Picasso.with(application).load(chatter.getAvatarUrl()).into(holder.avatarImageView);
        } else {
            holder.avatarImageView.setImageDrawable(null);
        }

    }

    @Override
    public long getItemId(int position) {
        //TODO: double check use of Message IDs, make sure they can be guaranteed.
        return mDataset.get(position).getTimestamp();
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void setDataset(ArrayList<Message> messages) {
        mDataset = messages;
        notifyDataSetChanged();
    }

}
package com.pentacog.fetchat.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pentacog.fetchat.FetChatApplication;
import com.pentacog.fetchat.R;
import com.pentacog.fetchat.fragments.ContactListFragment;
import com.pentacog.fetchat.fragments.NavigationDrawerFragment;
import com.pentacog.fetchat.models.Chatter;
import com.pentacog.fetchat.networking.WebServicesHandler;
import com.squareup.picasso.Picasso;


public class HomeActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private static final String TAG = FetChatApplication.TAG_PREFIX + HomeActivity.class.getSimpleName();

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    private Chatter mLoggedInUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if (savedInstanceState != null) {
            mLoggedInUser = savedInstanceState.getParcelable("User");
        }

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));


        LayoutInflater inflater = (LayoutInflater) this .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.app_bar_user, null);

        final TextView userNameTextView = (TextView) v.findViewById(R.id.textView);
        final ImageView userAvatarImageView = (ImageView) v.findViewById(R.id.imageView);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(v);
        }

//        login(userNameTextView, userAvatarImageView);

        if (mLoggedInUser == null) {
            /* Login */
            WebServicesHandler.postSession(new WebServicesHandler.LoginResponseHandler() {
                @Override
                public void onSuccess(int statusCode, final Chatter chatter) {
                    Log.d(TAG, "Session Success:" + statusCode);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            postLogin(chatter, userNameTextView, userAvatarImageView);
                        }
                    });


                }

                @Override
                public void onFailure(int statusCode, Error error) {
                    Log.d(TAG, "Session Failure:" + statusCode + ". " + error.getLocalizedMessage());
                }
            });
        }  else {
            if (userNameTextView != null) {
                userNameTextView.setText(mLoggedInUser.getNickname());
            }

            Picasso.with(this).load(mLoggedInUser.getAvatarUrl()).into(userAvatarImageView);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("User", mLoggedInUser);

    }

    private void postLogin(Chatter chatter, @Nullable final TextView userNameTextView, @Nullable final ImageView userAvatarImageView) {
        mLoggedInUser = chatter;

        FetChatApplication application = (FetChatApplication) getApplicationContext();
        application.setUser(chatter);
        application.getDataHandler().startDataPolling();

        application.getRoster().updateChatter(chatter.toJson());

        if (userNameTextView != null) {
            userNameTextView.setText(mLoggedInUser.getNickname());
        }

        Picasso.with(this).load(mLoggedInUser.getAvatarUrl()).into(userAvatarImageView);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, ContactListFragment.newInstance())
                .commit();
    }

//    public void onSectionAttached(int number) {
//        switch (number) {
//            case 1:
//                mTitle = getString(R.string.title_section1);
//                break;
//            case 2:
//                mTitle = getString(R.string.title_section2);
//                break;
//            case 3:
//                mTitle = getString(R.string.title_section3);
//                break;
//        }
//    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
//        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(false);
//        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.home, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void login(final TextView userNameTextView, final ImageView userAvatarImageView) {
        WebServicesHandler.login("username", "password", new WebServicesHandler.LoginResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Chatter chatter) {
                Log.d(TAG, "Login Success:" + statusCode);
                WebServicesHandler.postSession(new WebServicesHandler.LoginResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, final Chatter chatter) {
                        Log.d(TAG, "Session Success:" + statusCode);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                postLogin(chatter, userNameTextView, userAvatarImageView);
                            }
                        });
                    }

                    @Override
                    public void onFailure(int statusCode, Error error) {
                        Log.d(TAG, "Session Failure:" + statusCode + ". " + error.getLocalizedMessage());
                    }
                });
            }

            @Override
            public void onFailure(int statusCode, Error error) {
                Log.d(TAG, "Login Failure:" + statusCode + ". " + error.getLocalizedMessage());
            }
        });
    }

}

package com.pentacog.fetchat.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;

import com.pentacog.fetchat.FetChatApplication;
import com.pentacog.fetchat.R;
import com.pentacog.fetchat.fragments.ChatFragment;
import com.pentacog.fetchat.models.Chatter;
import com.pentacog.fetchat.models.Conversation;
import com.pentacog.fetchat.models.Message;
import com.pentacog.fetchat.networking.WebServicesHandler;

import java.util.ArrayList;


public class ChatActivity extends ActionBarActivity implements ChatFragment.OnMessageCreatedListener {

    private static final String TAG = FetChatApplication.TAG_PREFIX + ChatActivity.class.getSimpleName();

    private Conversation mConversation;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            long userId = intent.getLongExtra(FetChatApplication.EXTRA_USER_ID, 0);

            if (mConversation.getUserId() == userId) {
                ChatFragment fragment = (ChatFragment) getSupportFragmentManager().findFragmentById(R.id.container);
                fragment.reloadView();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        long userId = getIntent().getLongExtra("userId", 0);

        if (savedInstanceState == null) {

            Bundle bundle = new Bundle();
            bundle.putLong("userId", userId);
            ChatFragment fragobj = new ChatFragment();
            fragobj.setArguments(bundle);

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragobj)
                    .commit();
        }

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        FetChatApplication application = (FetChatApplication) getApplicationContext();
        Chatter chatter = application.getRoster().getChatterByUserId(userId);
        if (chatter != null) {
            actionBar.setTitle(chatter.getNickname());
        }

        mConversation = application.findOrCreateChat(userId);

        loadHistory(mConversation.getUserId());
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
        broadcastManager.registerReceiver(mReceiver, new IntentFilter(FetChatApplication.ACTION_MESSAGE));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
        broadcastManager.unregisterReceiver(mReceiver);
    }

    private void loadHistory(long userId) {
        WebServicesHandler.getHistory(userId, new WebServicesHandler.HistoryResponseHandler() {
            @Override
            public void onSuccess(int statusCode, final ArrayList<Message> messages) {
                mConversation.setMessages(messages);

                ChatFragment fragment = (ChatFragment) getSupportFragmentManager().findFragmentById(R.id.container);
                fragment.reloadView();
            }

            @Override
            public void onFailure(int statusCode, Error error) {
                Log.e(TAG, "Failed to load chat history: " + error.getLocalizedMessage());
            }
        });
    }

    public Conversation getConversation() {
        return mConversation;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMessageCreated(Message message) {
        mConversation.addMessage(message);

        ChatFragment fragment = (ChatFragment) getSupportFragmentManager().findFragmentById(R.id.container);
        fragment.reloadView();
    }

}

package com.pentacog.fetchat.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class Chatter implements Parcelable {

    public static final Parcelable.Creator<Chatter> CREATOR
            = new Parcelable.Creator<Chatter>() {
        public Chatter createFromParcel(Parcel in) {
            return new Chatter(in);
        }

        public Chatter[] newArray(int size) {
            return new Chatter[size];
        }
    };

    public static final String KEY_NICKNAME = "nickname";
    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_AVATAR_URL = "avatar_url";
    public static final String KEY_STATUS = "status";
    public static final String KEY_CHATSTATE = "chatstate";

    public static final String STATUS_UNAVAILABLE = "unavailable";
    public static final String STATUS_AVAILABLE = "available";
    public static final String STATUS_INVISIBLE = "invisible";

    private String nickname;
    private long userId;
    private String avatarUrl;
    private String status;
    private String chatstate;

    public Chatter(Parcel in) {
        this.nickname = in.readString();
        this.userId = in.readLong();
        this.avatarUrl = in.readString();
        this.status= in.readString();
        this.chatstate = in.readString();
    }

    public Chatter(JSONObject json) {
        this.nickname = json.optString(KEY_NICKNAME);
        this.userId = json.optLong(KEY_USER_ID);
        this.avatarUrl = json.optString(KEY_AVATAR_URL);
        this.status = json.optString(KEY_STATUS);
        this.chatstate = json.optString(KEY_CHATSTATE);
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        try {
            json.putOpt(KEY_NICKNAME, nickname);
            json.putOpt(KEY_USER_ID, userId);
            json.putOpt(KEY_AVATAR_URL, avatarUrl);
            json.putOpt(KEY_STATUS, status);
            json.putOpt(KEY_CHATSTATE, chatstate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getChatstate() {
        return chatstate;
    }

    public void setChatstate(String chatstate) {
        this.chatstate = chatstate;
    }

    public void update(JSONObject json) {
        if (!json.isNull(KEY_NICKNAME))
            this.nickname = json.optString(KEY_NICKNAME);
        if (!json.isNull(KEY_USER_ID))
            this.userId = json.optLong(KEY_USER_ID);
        if (!json.isNull(KEY_AVATAR_URL))
            this.avatarUrl = json.optString(KEY_AVATAR_URL);
        if (!json.isNull(KEY_STATUS))
            this.status = json.optString(KEY_STATUS);
        if (!json.isNull(KEY_CHATSTATE))
            this.chatstate = json.optString(KEY_CHATSTATE);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nickname);
        dest.writeLong(userId);
        dest.writeString(avatarUrl);
        dest.writeString(status);
        dest.writeString(chatstate);
    }

}


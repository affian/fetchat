package com.pentacog.fetchat.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Conversation {

    public static final String KEY_WITH_USER_ID = "with_user_id";
    public static final String KEY_MESSAGES = "messages";
    public static final String KEY_UNREAD = "unread";
    public static final String KEY_IS_ACTIVE = "is_active";

    private long userId;
    private int unread;
    private ArrayList<Message> messages;
    private long isActive;

    public Conversation(JSONObject json) throws JSONException {
        this.userId = json.getLong(KEY_WITH_USER_ID);
        this.unread = json.getInt(KEY_UNREAD);
        this.isActive = json.getLong(KEY_IS_ACTIVE);
        JSONArray messages = json.getJSONArray(KEY_MESSAGES);
        this.messages = new ArrayList<>();
    }

    public Conversation(long userId) {
        this.userId = userId;
        this.unread = 0;
        this.isActive = System.currentTimeMillis();
        this.messages = new ArrayList<>();
    }

    public long getUserId() {
        return userId;
    }

    public int getUnread() {
        return unread;
    }

    public void setUnread(int unread) {
        this.unread = unread;
    }

    public long getIsActive() {
        return isActive;
    }

    public void setIsActive(long isActive) {
        this.isActive = isActive;
    }

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }

    public boolean addMessage(Message message) {
        return this.messages.add(message);
    }

}

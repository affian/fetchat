package com.pentacog.fetchat.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Roster {

    private ArrayList<OnRosterChangeListener> listeners;
    private HashMap<Long, Chatter> rosterMap;

    public Roster() {
        rosterMap = new HashMap<>();
        listeners = new ArrayList<>();
    }

    public boolean addOnRosterChangeListener(OnRosterChangeListener listener) {
        return listeners.add(listener);
    }

    public boolean removeOnRosterChangeListener(OnRosterChangeListener listener) {
        return listeners.remove(listener);
    }

    public Chatter removeChatter(long userId) {
        Chatter chatter = getChatterByUserId(userId);
        return rosterMap.remove(userId);
    }

    public Chatter getChatterByUserId(long userId) {
        return rosterMap.get(userId);
    }

    public void updateRoster(JSONArray rosterJson) {
        for (int i = 0; i < rosterJson.length(); i++) {
            findOrUpdateChatter(rosterJson.optJSONObject(i));
        }
        updateListeners();
    }

    public void updateChatter(JSONObject chatterJson) {
        findOrUpdateChatter(chatterJson);
        updateListeners();
    }

    private Chatter findOrUpdateChatter(JSONObject chatterJSON) {
        Chatter chatter = getChatterByUserId(chatterJSON.optLong("user_id", -1));

        if (chatter == null) {
            chatter = new Chatter(chatterJSON);
            rosterMap.put(chatter.getUserId() ,chatter);
        } else {
            chatter.update(chatterJSON);
        }

        return chatter;
    }

    private void updateListeners() {
        ArrayList<Chatter> roster = getRosterList();
        for (OnRosterChangeListener listener:listeners) {
            listener.onRosterChange(roster);
        }
    }

    public ArrayList<Chatter> getRosterList() {
        return new ArrayList<>(rosterMap.values());
    }

    public interface OnRosterChangeListener {
        void onRosterChange(ArrayList<Chatter> chatters);
    }

}

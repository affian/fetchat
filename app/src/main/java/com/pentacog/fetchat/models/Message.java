package com.pentacog.fetchat.models;

import com.pentacog.fetchat.FetChatApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class Message {

    public static final String KEY_FROM = "from";
    public static final String KEY_TO= "to";
    public static final String KEY_BODY = "body";
    public static final String KEY_TIMESTAMP = "timestamp";
    public static final String KEY_SEQ = "seq";
    public static final String KEY_ID = "id";

    private long from;
    private long to;
    private String body;
    private long timestamp;
    private Date date;
    private int seq;
    private long id;

    public Message(long from, long to, String body) {
        this.from = from;
        this.to = to;
        this.body = body;
        this.timestamp = System.currentTimeMillis();
        this.date = new Date(this.timestamp);
        this.seq = FetChatApplication.nextSeq();
        this.id = 0;
    }

    public Message(long to, String body) {
        this(0, to, body);
    }

    public Message(JSONObject json) throws JSONException {
        this.from = json.getLong(KEY_FROM);
        this.to = json.getLong(KEY_TO);
        this.body = json.getString(KEY_BODY);
        this.timestamp = json.getLong(KEY_TIMESTAMP);
        this.date = new Date(this.timestamp);
        this.seq = json.getInt(KEY_SEQ);
        this.id = json.optLong(KEY_ID);
    }

    public JSONObject toJson() throws JSONException{
        JSONObject json = new JSONObject();
        json.put(KEY_FROM, (from == 0)?null:from);
        json.put(KEY_TO, to);
        json.put(KEY_BODY, body);
        json.put(KEY_TIMESTAMP, timestamp);
        json.put(KEY_SEQ, seq);
        json.put(KEY_ID, (id == 0)?null:id);

        return json;
    }

    public long getFrom() {
        return from;
    }

    public long getTo() {
        return to;
    }

    public String getBody() {
        return body;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public int getSeq() {
        return seq;
    }

    public long getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

}

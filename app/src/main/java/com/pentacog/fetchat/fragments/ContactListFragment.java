package com.pentacog.fetchat.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pentacog.fetchat.DividerItemDecoration;
import com.pentacog.fetchat.FetChatApplication;
import com.pentacog.fetchat.R;
import com.pentacog.fetchat.RecyclerItemClickListener;
import com.pentacog.fetchat.activities.ChatActivity;
import com.pentacog.fetchat.adapters.ChatterAdapter;
import com.pentacog.fetchat.models.Chatter;
import com.pentacog.fetchat.models.Roster;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class ContactListFragment extends Fragment {

    private static final String TAG = FetChatApplication.TAG_PREFIX + ContactListFragment.class.getSimpleName();

    private RecyclerView mRecyclerView;
    private ChatterAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Chatter> mChatters = new ArrayList<>();
    private RosterChangeListener mRosterListener;

    public ContactListFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_main, container, false);
        FetChatApplication application = (FetChatApplication) getActivity().getApplicationContext();

        setChatters(application.getRoster().getRosterList());

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), null));

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new ChatterAdapter(getActivity(), mChatters);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Chatter user = mChatters.get(position);

                        Intent i = new Intent(getActivity(), ChatActivity.class);
                        i.putExtra("userId", user.getUserId());

                        startActivity(i);
                    }
                })
        );

        mRosterListener = new RosterChangeListener(getActivity());
        application.getRoster().addOnRosterChangeListener(mRosterListener);

        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        FetChatApplication application = (FetChatApplication) getActivity().getApplication();
        application.getRoster().removeOnRosterChangeListener(mRosterListener);
    }

    public void setChatters(ArrayList<Chatter> chatters) {
        this.mChatters = chatters;
    }

    public static Fragment newInstance() {
        return new ContactListFragment();
    }

    private class RosterChangeListener implements Roster.OnRosterChangeListener {

        WeakReference<Activity> weakActivity;

        public RosterChangeListener(Activity activity) {
            weakActivity = new WeakReference<>(activity);
        }

        @Override
        public void onRosterChange(final ArrayList<Chatter> chatters) {
            if (weakActivity.get() == null) return;
            //TODO: weak reference implementation introduced bug, contact list not receiving updates.
            weakActivity.get().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ContactListFragment.this.setChatters(chatters);
                    mAdapter.updateDataSet(mChatters);
                }
            });
        }
    }

}

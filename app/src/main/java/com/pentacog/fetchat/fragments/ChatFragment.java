package com.pentacog.fetchat.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.pentacog.fetchat.DividerItemDecoration;
import com.pentacog.fetchat.FetChatApplication;
import com.pentacog.fetchat.R;
import com.pentacog.fetchat.activities.ChatActivity;
import com.pentacog.fetchat.adapters.MessageAdapter;
import com.pentacog.fetchat.models.Conversation;
import com.pentacog.fetchat.models.Message;
import com.pentacog.fetchat.networking.WebServicesHandler;

public class ChatFragment extends Fragment {

    private long userId;

    private RecyclerView mRecyclerView;
    private MessageAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;

    private EditText mEditText;
    private ImageButton mSendButton;

    public ChatFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_chat, container, false);

        userId = getArguments().getLong("userId");

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mEditText = (EditText) rootView.findViewById(R.id.editText);
        mSendButton = (ImageButton) rootView.findViewById(R.id.sendButton);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), null));

        FetChatApplication application = (FetChatApplication) getActivity().getApplication();
        Conversation conversation = application.findOrCreateChat(userId);

        mAdapter = new MessageAdapter(getActivity(), conversation.getMessages());
        mRecyclerView.setAdapter(mAdapter);

        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageText = mEditText.getText().toString();
                if (!TextUtils.isEmpty(messageText)) {
                    Message message = buildMessage(messageText);
                    WebServicesHandler.postMessage(message);

                    OnMessageCreatedListener listener = (OnMessageCreatedListener) getActivity();
                    listener.onMessageCreated(message);

                    mEditText.setText("");
                }
            }
        });

        return rootView;
    }

    private Message buildMessage(String body) {
        ChatActivity activity = (ChatActivity) getActivity();
        Conversation conversation = activity.getConversation();

        return new Message(conversation.getUserId(), body);
    }

    public void reloadView() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                FetChatApplication application = (FetChatApplication) getActivity().getApplication();
                Conversation conversation = application.findOrCreateChat(userId);
                mAdapter.setDataset(conversation.getMessages());
                if (mAdapter.getItemCount() > 0)
                    mRecyclerView.smoothScrollToPosition(mAdapter.getItemCount() - 1);
            }    });
    }

    public interface OnMessageCreatedListener {
        void onMessageCreated(Message message);
    }

}